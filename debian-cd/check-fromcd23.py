#!/usr/bin/env python

import re

names = { }

def clean (s) :
    s = s.strip ()
    s = re.sub ("//.*$", "", s)
    return s

input = open ("popularity-contest", 'r')
for line in input :
   line = clean (line)
   names [line] = True

input = open ("interesting-fromcd23", 'r')
for line in input :
   line = clean (line)
   if line != "" :
      if not line in names :
         print line


